<?php

    global $wp_query;
	ob_start();
	$header = ob_get_contents();
	ob_end_clean();
	if ( isset( $wp_query->query_vars['dfp_tag'] ) && !empty( $wp_query->query_vars['dfp_tag'] ) ) {

		$the_dfp_tag = strtolower($wp_query->query_vars['dfp_tag']);
		$matched_dfp_tag_value = '';
		$dfp_tags = get_option('dfp_tags');
		$dfp_tags_desc = get_option('dfp_tags_desc');
		$position = 0; 
		$matched_dfp_tag_desc = '';

		foreach( $dfp_tags as $d ) {

			$dfp_tag = strtolower( str_replace( ' ', '-', str_replace( '&', '-', $d ) ) );

			if( $dfp_tag == $the_dfp_tag ) {

				$matched_dfp_tag_value = $d;
				break;

			}

			++$position; 
		}

		$matched_dfp_tag_desc = $dfp_tags_desc[$position];

		if( !empty( $matched_dfp_tag_value ) ) {

		$paged = explode('/', $_SERVER['REQUEST_URI']);

		if( !empty( $paged[4] ) ) {

			$paged = $paged[4];

		} else {

			$paged = 1;

		}

		$args = array ( 
			'meta_key' => 'post_dfp_tag', 
			'posts_per_page' => '14',
			'paged' => $paged, 
			'meta_value'=> $matched_dfp_tag_value , 
			'orderby'=>'date', 
			'order'=>'desc', 
			'meta_compare'=>'='
		);
		query_posts( $args );

		} else {
			//redirect to home page 
			header('location: ' . site_url());
			exit;
		}
	} else {
		//redirect to home page
		header('location: ' . site_url());
		exit;
    }
	echo preg_replace( 
		'/<meta property="og:url" content="(.+?)"\s?\/>/', 
		'<meta property="og:url" content="'.site_url().'/section/'.strtolower(str_replace(' ', '-', str_replace('&', '-', $matched_dfp_tag_value))).'/" />', 
		preg_replace( 
			'/<meta property="og:title" content="(.+?)"\s?\/>/', 
			'<meta property="og:title" content="'.$matched_dfp_tag_value.'" />', 
			preg_replace( 
				'/<title>(.+?)<\/title>/', 
				'<title>'.$matched_dfp_tag_value.'</title>', 
				$header 
			)
		)
    );
?>