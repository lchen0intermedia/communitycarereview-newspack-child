<?php
	/*
	Template name: DFP Tag
	* @link https://developer.wordpress.org/themes/basics/template-hierarchy/
	*
	* @package Newspack
	*/
?>

<?php get_header(); ?>

<?php require_once 'dfp-tag-scripts.php'; ?>

<section id="primary" class="content-area">
	<!-- .page-header -->
	<header class="page-header">
		<?php the_archive_description( '<div class="taxonomy-description">', '</div>' ); ?>
		<h1 class="page-title">
			<?php echo $matched_dfp_tag_value;?>
		</h1>
		<hr style="width:30px;height:1px;background-color:#dae0eb;">
		<div class="taxonomy-description">
			<p><?php echo $matched_dfp_tag_desc;?></p>
		</div>
	</header>
	<!-- .page-header -->
	<main id="main" class="site-main">
		<?php if ( have_posts() ) : $post_count = 0; ?>  
			<?php
				// Start the Loop.
				while ( have_posts() ) : 
					$post_count++;
					the_post();
					if ( 1 === $post_count ) { 
						get_template_part( 'template-parts/content/content', 'excerpt' );
					} else {
						get_template_part( 'template-parts/content/content', 'archive' );
					}
					$the_post_type = get_post_type( get_the_ID() ); 
					if( $the_post_type == 'sponsoredcontent' ) {
			?>
					<script type="text/javascript">
					<?php 
						if(function_exists('intermedia_ga_event')) {
							echo intermedia_ga_event( 'Impression', 'Sponsor Content Impression', get_the_title( get_the_ID() ) );
						}
					?>
					</script>
					<?php } ?> 
				<?php endwhile;
				// End the loop.
			// Previous/next page navigation.
			newspack_the_posts_navigation();
		// If no content, include the "No posts found" template.
		else :
			get_template_part( 'template-parts/content/content', 'none' );
		endif;
	?>
	</main><!-- #main -->
	<?php get_sidebar(); ?>
</section><!-- #primary -->

<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery('body').removeClass('page');	
		jQuery('body').addClass('archive');	
	});
</script>

<?php 
get_footer();
