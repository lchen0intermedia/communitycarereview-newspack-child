<?php
/**
 * Theme functions
 */
add_action( 'wp_enqueue_scripts', 'newspack_child_enqueue_styles' );
function newspack_child_enqueue_styles() {

    $parent_style = 'parent-style';

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
    // include the css file
    $cssFilePath = glob( get_stylesheet_directory() . '/css/build/main.min.*' );
    $cssFileURI = get_stylesheet_directory_uri() . '/css/build/' . basename($cssFilePath[0]);
    wp_enqueue_style( 'child-style-min', $cssFileURI );
    // include the javascript file
    $jsFilePath = glob( get_stylesheet_directory() . '/js/build/app.min.*.js' );
    $jsFileURI = get_stylesheet_directory_uri() . '/js/build/' . basename($jsFilePath[0]);
    wp_enqueue_script( 'child-scripts-min', $jsFileURI , null , null , true );
}
// remove default guttenberg block editor stylesheet
//  https://wpassist.me/how-to-remove-block-library-css-from-wordpress/
function wpassist_remove_block_library_css(){
    wp_dequeue_style( 'wp-block-library' );
} 
add_action( 'wp_enqueue_scripts', 'wpassist_remove_block_library_css' );

/**********************************************************************************************/
/**
 * Custom widget for the header
 *
 */
function ageingagenda_widgets_init() {

	register_sidebar(
		array(
			'name'          => __( 'Header', 'newspack' ),
			'id'            => 'header',
			'description'   => __( 'Add widgets here to appear in your header.', 'newspack' ),
			'before_widget' => '<section id="leaderboard-widget">',
			'after_widget'  => '</section>'
		)
	);

}
add_action( 'widgets_init', 'ageingagenda_widgets_init' );


// Function to add shortcode to newspack_social_menu_header
  function subscribe_social_media_newspack () {
      $output = newspack_social_menu_header();
    return $output;
}
add_shortcode('subscribe_social_media_newspack', 'subscribe_social_media_newspack');

//Remove “Category:”, “Tag:”, “Author:” from the_archive_title
add_filter( 'get_the_archive_title', function ($title) {

    if ( is_category() ) {

            $title = single_cat_title( '', false );

        } elseif ( is_tag() ) {

            $title = single_tag_title( '', false );

        } elseif ( is_author() ) {

            $title = '<span class="vcard">' . get_the_author() . '</span>' ;

        }

    return $title;

}, 11, 1);

//DISABLE RELATED POSTS UNDER POSTS
add_filter( 'rp4wp_append_content', '__return_false' );

function wpb_adding_scripts() {
 
        wp_enqueue_script(
                'TweenMax.min',
                'https://cdnjs.cloudflare.com/ajax/libs/gsap/2.1.2/TweenMax.min.js'
        );

        wp_enqueue_script(
                'ScrollMagic.min',
                'https://www.barsclubs.com.au/wp-content/themes/bars-clubs-wp-builder/src/js/ScrollMagic.min.js?ver=5.2.3'
        );

        wp_enqueue_script(
                'animation.gsap',
                'https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/plugins/animation.gsap.js'
        );
    
} 
add_action( 'wp_enqueue_scripts', 'wpb_adding_scripts' );



// Add a custom controller
add_filter('json_api_controllers', 'add_my_controller');

function add_my_controller($controllers) {
  // Corresponds to the class JSON_API_MyController_Controller
  $controllers[] = 'MyController';
  return $controllers;
}
/* DFP TAGS FILTER */
//for adding a body class to a the DFP page template:
add_filter( 'body_class', 'custom_class_body' );
function custom_class_body( $classes ) {
    if ( is_page_template( 'dfp-tag-template-page.php' ) ) {
        $classes[] = 'archive';
        // List of the only WP generated classes that are not allowed
        $blacklist = array( 'page' );
        $classes = array_diff( $classes, $blacklist );
    }
    return $classes;
}