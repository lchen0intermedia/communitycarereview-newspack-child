# Welcome to Newspack Child Theme

Basic Child Theme for Newspack Theme Framework: [[https://github.com/Automattic/newspack-theme](https://github.com/Automattic/newspack-theme))

#  How it works
Newspack Child Theme shares with the parent theme all PHP files and adds its own functions.php on top of the Newspack parent theme's functions.php.

**IT DOES NOT LOAD THE PARENT THEMES CSS FILE(S)!**  Instead it uses the Newspack Parent Theme as a dependency via npm and compiles its own CSS file from it.

> **Tip:** **Newspack Child Theme** uses the **Enqueue** method to load and sort the CSS file the right way instead of the old **@import** method...
> 
## How to install Newspack on your site

The easiest way to do so is to [download the latest plugin release](https://github.com/Automattic/newspack-plugin/releases) and [the latest theme release](https://github.com/Automattic/newspack-theme/releases). Upload them using the plugin or theme installer in your WordPress admin interface. To take full advantage of Newspack, the plugin and theme should be run together, but each should also work fine individually.

> **Important!!** Don't install the plugin **AMP** that comes preinstalled with the theme because it doesn't allow to add custom JavaScript to pages and Post. This plugin will disable all the JS scripts by default.

## Child theme Installation

1.  **Upload / clone** the Newspack Child Theme **folder / repository** to your wp-content/themes directory
2.  Go into your WP admin backend
3.  Go to "Appearance -> Themes"
4.  Activate the Newspack Child Theme

## Editing

Add your own CSS styles to  `/css/src/theme.scss`  or import you own files into  `/css/src/main.scss`

To overwrite Newspack variables just add your own value to:  `/css/src/variables-site/_variables-site.scss`. In the same folder you can copy all the existing variables from several .scss files (these files are not imported by default) and paste them in `_variables-site.scss` for modifying.

> **Tip:** For example, the "$color__primary" variable is used by **Newspack** to setup the primary color on the site.

Add your own color like:  `$color__primary: #dddddd;`  in  `/css/src/variables-site/_variables-site.scss`  to overwrite it. This change will automatically apply to all elements that use the $color__primary variable.

It will be outputted into:  `/css/build/main.min.[hash].css` 

So you have one clean CSS file at the end and just one request.

Add your own JS scripts to  `/js/src/app.js` 

It will be outputted into:  `/js/build/app.min.[hash].js` 

> **Important!!** Disable the plugin **AMP** if this is active because it doesn't allow to add custom JavaScript to pages and Post. This plugin will disable all the JS scripts by default.

## Developing With NPM, Webpack and SASS

### Installing Dependencies

-   Make sure you have installed Node.js on your computer globally
-   Open your terminal and browse to the location of your Child theme copy
-   Run:  `$ npm install`

### Running

To work and compile your Sass and JS files on the fly start:

-   `$ npx webpack --watch`


##  Organizing the Source Code

I created a **JS** directory and a **CSS** directory inside of the root folder. Each of these directories had a **src** folder, which contained the source code, and a **build** folder, which acted as the build target for the **Webpack** compiled assets.