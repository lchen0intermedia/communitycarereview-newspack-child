<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Newspack
 */

?>

	<?php do_action( 'before_footer' ); ?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
            <section class="optin-form">
                <div class="optin-form-container">
                    <?php //echo do_shortcode('[et_bloom_inline optin_id="optin_4"]'); ?>
                    <!-- start footer subscribe form -->
                    <div class="et_bloom_inline_form et_bloom_optin et_bloom_make_form_visible et_bloom_optin_4">
                        <div class="et_bloom_form_container  with_edge carrot_edge et_bloom_form_text_dark et_bloom_form_right et_bloom_inline_2_fields">
                            <div class="et_bloom_form_container_wrapper clearfix">
                                <div class="et_bloom_header_outer">
                                    <div class="et_bloom_form_header et_bloom_header_text_light">
                                        <img width="610" height="407" src="https://cdn.communitycarereview.com.au/wp-content/uploads/2019/02/30113449/Care-worker-610x407.jpg" class="et_bloom_hide_mobile et_bloom_image_fadein et_bloom_image" alt="" loading="lazy" srcset="https://cdn.communitycarereview.com.au/wp-content/uploads/2019/02/30113449/Care-worker-610x407.jpg 610w, https://cdn.communitycarereview.com.au/wp-content/uploads/2019/02/30113449/Care-worker-300x200.jpg 300w, https://cdn.communitycarereview.com.au/wp-content/uploads/2019/02/30113449/Care-worker-1024x683.jpg 1024w, https://cdn.communitycarereview.com.au/wp-content/uploads/2019/02/30113449/Care-worker-768x512.jpg 768w, https://cdn.communitycarereview.com.au/wp-content/uploads/2019/02/30113449/Care-worker-525x350.jpg 525w, https://cdn.communitycarereview.com.au/wp-content/uploads/2019/02/30113449/Care-worker-1536x1024.jpg 1536w, https://cdn.communitycarereview.com.au/wp-content/uploads/2019/02/30113449/Care-worker-2048x1365.jpg 2048w, https://cdn.communitycarereview.com.au/wp-content/uploads/2019/02/30113449/Care-worker-1200x800.jpg 1200w, https://cdn.communitycarereview.com.au/wp-content/uploads/2019/02/30113449/Care-worker-1568x1045.jpg 1568w" sizes="(max-width: 610px) 100vw, 610px">
                                        <div class="et_bloom_form_text">
                                            <h2>Subscribe To Our Newsletter</h2><p>Join our mailing list to receive the latest news and updates from our team.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="et_bloom_form_content et_bloom_2_fields">
                                    <?php echo do_shortcode('[gravityform id=3 title=false description=false ajax=true]');?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end footer subscribe form -->
                </div>
            </section>
		<?php //get_template_part( 'template-parts/footer/footer', 'branding' ); ?>
		<?php get_template_part( 'template-parts/footer/footer', 'widgets' ); ?>

		<div class="site-info">
			<div class="wrapper">
				<?php $blog_info = get_bloginfo( 'name' ); ?>
				<?php if ( ! empty( $blog_info ) ) : ?>
					&copy; <?php echo esc_html( date( 'Y' ) ); ?> <?php bloginfo( 'name' ); ?>.
				<?php endif; ?>

<!--				<a href="<?php //echo esc_url( __( 'https://intermedia.com.au/', 'newspack' ) ); ?>" class="imprint">
					<?php
					/* translators: %s: WordPress. */
					//printf( esc_html__( 'Powered by %s.', 'newspack' ), 'Intermedia' );
					?>
				</a>-->

				<?php
				if ( function_exists( 'the_privacy_policy_link' ) ) {
					the_privacy_policy_link( '', '<span role="separator" aria-hidden="true"></span>' );
				}

//				if ( ! is_active_sidebar( 'footer-1' ) ) {
					newspack_social_menu_footer();
//				}
				?>
			</div><!-- .wrapper -->
		</div><!-- .site-info -->
	</footer><!-- #colophon -->

</div><!-- #page -->

<?php wp_footer(); ?>

<!-- The Modal FOR SAVE FOR LATER -->
<div id="saveforlaterModal" class="modal">
    <!-- Modal content -->
    <div id="save_for_later_wrapper" class="modal-content">
        <span class="close">&times;</span>
        <h3>Your Saved Articles</h3>
        <hr>
        <?php echo do_shortcode( '[simplicity-saved-for-later]' ); ?>
    </div>
</div>
<script>
    // Get the modal
    var modalSFL = document.getElementById("saveforlaterModal");
    // Get the button that opens the modal
    var btnSFL = document.getElementsByClassName("rs-saved-trigger");
    // Get the <span> element that closes the modal
    var spanSFL = document.getElementsByClassName("close")[0];
    var btnSFLnumber = btnSFL.length;
    for (var i = 0; i < btnSFLnumber; i++) {
        // When the user clicks the button, open the modal 
        btnSFL[i].addEventListener('click', function(event) {
            event.preventDefault();
            modalSFL.style.display = "block";
        },false);
    };  
    // When the user clicks on <span> (x), close the modal
    spanSFL.onclick = function() {
        modalSFL.style.display = "none";
    };
    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target === modalSFL) {
            modalSFL.style.display = "none";
        }
    };
</script>
<script>
        jQuery(document).ready(function($){
            // init controller
            var controller = new ScrollMagic.Controller({globalSceneOptions: {triggerHook: "onEnter", duration: "150%"}});
            $(window).load(function() {
                new ScrollMagic.Scene({triggerElement: "#parallaxContent"})
                .setTween("#parallaxContent > div", {y: "30%", ease: Linear.easeNone})
                .addTo(controller);
            });
        });
    </script>
</body>
</html>
